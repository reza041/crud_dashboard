<?php

include '../connection.php';

$data_siswa;
$data_jumlah = count($data_siswa);
$data_kosong = 1000 - $data_jumlah;



$total = 0;
foreach($data_siswa as $num)
{
    $total+=$num['harga'];
}


?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <!--Import icon fontawesome-->
	  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.1/css/all.css" integrity="sha384-vp86vTRFVJgpjF9jiIGPEEqYqlDwgyBgEF109VFjmqGmIY/Y4HV4d3Gp2irVfcrp" crossorigin="anonymous">
    <!-- Import Favicon -->
    <link rel="shortcut icon" href="asset/img/bus.png" type="image/x-icon" class="rounded-circle">
    <!-- Import Bootstrap -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="/resources/demos/style.css">
    <!--Import dari materialize.css1-->
    <script src="https://code.jquery.com/jquery-3.1.0.js"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.4/Chart.min.js"
            integrity="sha512-d9xgZrVZpmmQlfonhQUvTR7lMPtO7NkZMkA0ABN3PHCbKA5nqylQ/yWlFAyY6hYgdF1Qh6nYiuADWwKB4C2WSw=="
            crossorigin="anonymous">
    </script>
    <title>E-Commers</title>
</head>

<style type="text/css">
body {
    background-image: linear-gradient(to top left,#FF0000,#FA8072);
}
.bg{
	background: linear-gradient(to top left,#FF0000,#FA8072);
}
nav{
	background-image: linear-gradient(to top right,#FA8072,#FF0000); 
	padding-left: 300px;
}

.card{
    background-color: rgba(0,0,0,0);
}
.content{
	padding-left: 300px;
	height: 800px;
}
.card-bg{
	background: rgba(0,0,0,0);
}

#grafik{
    background-color: #ffcdd2;
}

@media only screen and (max-width: 992px){
	.content,nav{
		padding-left: 0;
	}
}

#overflow{
          background-color: salmon;
          width: 20rem;
          height: 40rem;
          border: 1px solid black;
          overflow: scroll;
        }
</style>

<!-- slide out -->

<script type="text/javascript">
	$(document).ready(function(){
		$('.sidenav').sidenav();
	});
</script>

<body>

 <!-- navbar -->

<div class="navbar-fixed">
  <nav>
    <div class="nav-wrapper">
      <h2 class="brand-logo center my-3">Dashboard</h2>
      <a href="#" data-target="slide-out" class="sidenav-trigger"><i class="fas fa-bars fa-2x"></i></a>		
    </div>
  </nav>
</div>
    
    <!-- sidenav -->

<ul class="sidenav sidenav-fixed bg" id="slide-out">
	<li>
		<div class="user-view">
			<div class="background">
				<img src="../asset/images/bg.jpg" width="100%">
			</div>
				<a style="text-decoration: none;" href="https://github.com/reza04"><img src="../asset/images/pp.jpg" class="circle"></a>
				<a style="text-decoration: none;" href="https://github.com/reza04" class="white-text name">Reza Aditya Pratama</a>
				<a style="text-decoration: none;" href="mailto:rezaaditya9132@gmail.com" class="white-text email">rezaaditya9132@gmail.com</a>
		</div>	
  </li>
  <li><a style="text-decoration: none;" href="../index.php" class="white-text"><i class="fas fa-home fa-2x"></i>Home</a></li>
  <li><a style="text-decoration: none;" href="index.php" class="white-text"><i style="font-size: 2rem;" class="far fa-credit-card"></i>Dashboard</a></li>
  <li><a style="text-decoration: none;" href="../delete.php" class="white-text"><i style="font-size: 2rem;" class="fas fa-bars"></i>Menu</a></li>
  <li><a style="text-decoration: none;" href="../tambah.php" class="white-text"><i class="fas fa-folder-plus" style="font-size: 2rem;"></i>Tambah Barang</a></li>
  
</ul>

<div class="content bg white-text ">
        <div class="row my-4">
            <div class="col-lg-4 col-sm-12">
                <div class="card text-white" style="max-width: 20rem;">
                    <div class="card-body">
                        <h5 class="card-title">Total Gerai Terpakai</h5>
                        <h4><?php echo $data_jumlah; ?></h4>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-sm-12">
                <div class="card text-white" style="max-width: 20rem;">
                    <div class="card-body">
                        <h5 class="card-title">Total Omset Penjualan</h5>
                        <h4><?php echo "Rp.".number_format($total); ?></h4>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-sm-12">
                <div class="card text-white" style="max-width: 20rem;">
                    <div class="card-body">
                        <h5 class="card-title">Total Gerai Kosong</h5>
                        <h4><?php echo $data_kosong; ?></h4>
                    </div>
                </div>
            </div>
            <div class="col-lg-8 col-sm-12">
                <h5>Grafik Harga Barang</h5>
                <div id="grafik" class="card text-dark" style="max-width: 40rem;">
                    <div class="card-body">
                    <canvas id="myChart" width="300" height="300"></canvas>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-sm-12">
                <h5>Data Barang</h5>
                <div id="overflow" style="background-color: #ffcdd2;">
                    <?php foreach($data_siswa as $key):?>
                        <div class="card shadow mt-3  m-2">
                            <div class="card-body" style="background-color: #FF0000;">
                                <h5 class="card-title"><?php echo $key['nama']  ?></h5>
                                <img class="card-img-top p-2" src="../uploads/<?php echo $key['file']  ?>" alt="" width="70px" height="125px">
                                <p class="card-text"><?php echo "Harga : Rp.".number_format($key["harga"]); ?></p>
                                <h6 class="card-text"><small class="text-white"><?php echo "jenis Produk :".$key['jenis_produk']  ?></small></h6>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </div>

<script>
    
          
var ctx = document.getElementById('myChart').getContext('2d');
    function dynamicColors() {
        var r = Math.floor(Math.random() * 255);
        var g = Math.floor(Math.random() * 255);
        var b = Math.floor(Math.random() * 255);
        return "rgba(" + r + "," + g + "," + b + ", 0.5)";
    }
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: [<?php foreach($data_siswa as $key) { echo '"'.$key['nama']. '",'; } ?>],
        datasets: [{
            label: '# of Barang',
            data:  [<?php foreach($data_siswa as $key1) { echo '"'.$key1['harga']. '",'; } ?>],
            backgroundColor: dynamicColors,
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
});
</script>
   



<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js" integrity="sha384-w1Q4orYjBQndcko6MimVbzY0tgp4pWB4lZ7lr30WKz0vr/aWKhXdBNmNb5D92v7s" crossorigin="anonymous"></script> -->
</body>
</html>